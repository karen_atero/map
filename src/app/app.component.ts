import {Component, OnInit} from '@angular/core';
import * as L from 'leaflet';
import 'leaflet.vectorgrid';
import {IconGenerator} from '../utils/icon-generator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  markerCount = 10;
  bnds: any;
  map: L.Map;
  options = {
    layers: [

      L.vectorGrid.protobuf('https://maps.tilehosting.com/data/v3/{z}/{x}/{y}.pbf?key=BuNi4FPIgsaSVnVlaLoQ', {
        vectorTileLayerStyles: {
          water: {
            fill: true,
            weight: 1,
            fillColor: '#06cccc',
            color: '#06cccc',
            fillOpacity: 0.2,
            opacity: 0.4,
          },
          admin: {
            weight: 1,
            fillColor: 'pink',
            color: 'pink',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          waterway: {
            weight: 1,
            fillColor: '#2375e0',
            color: '#2375e0',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          landcover: {
            fill: true,
            weight: 1,
            fillColor: '#53e033',
            color: '#53e033',
            fillOpacity: 0.2,
            opacity: 0.4,
          },
          landuse: {
            fill: true,
            weight: 1,
            fillColor: '#e5b404',
            color: '#e5b404',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          park: {
            fill: true,
            weight: 1,
            fillColor: '#84ea5b',
            color: '#84ea5b',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          boundary: {
            weight: 1,
            fillColor: '#c545d3',
            color: '#c545d3',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          aeroway: {
            weight: 1,
            fillColor: '#51aeb5',
            color: '#51aeb5',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          road: {	// mapbox & mapzen only
            weight: 1,
            fillColor: '#f2b648',
            color: '#f2b648',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          tunnel: {	// mapbox only
            weight: 0.5,
            fillColor: '#f2b648',
            color: '#f2b648',
            fillOpacity: 0.2,
            opacity: 0.4,
// 					dashArray: [4, 4]
          },
          bridge: {	// mapbox only
            weight: 0.5,
            fillColor: '#f2b648',
            color: '#f2b648',
            fillOpacity: 0.2,
            opacity: 0.4,
// 					dashArray: [4, 4]
          },
          transportation: {	// openmaptiles only
            weight: 0.5,
            fillColor: '#f2b648',
            color: '#f2b648',
            fillOpacity: 0.2,
            opacity: 0.4,
// 					dashArray: [4, 4]
          },
          transit: {	// mapzen only
            weight: 0.5,
            fillColor: '#f2b648',
            color: '#f2b648',
            fillOpacity: 0.2,
            opacity: 0.4,
// 					dashArray: [4, 4]
          },
          building: {
            fill: true,
            weight: 1,
            fillColor: '#2b2b2b',
            color: '#2b2b2b',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          water_name: {
            weight: 1,
            fillColor: '#022c5b',
            color: '#022c5b',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          transportation_name: {
            weight: 1,
            fillColor: '#bc6b38',
            color: '#bc6b38',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          place: {
            weight: 1,
            fillColor: '#f20e93',
            color: '#f20e93',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          housenumber: {
            weight: 1,
            fillColor: '#ef4c8b',
            color: '#ef4c8b',
            fillOpacity: 0.2,
            opacity: 0.4
          },
          poi: {
            weight: 1,
            fillColor: '#3bb50a',
            color: '#3bb50a',
            fillOpacity: 0.2,
            opacity: 0.4
          }

        },
        maxZoom: 18,
        attribution: '...'
      })

    ],
    zoom: 11,
    center: L.latLng(46.879966, 40.726909)
  };

  constructor(
      private generator: IconGenerator
  ) {
  }

  ngOnInit(): void {

  }

  placeMarkers() {
    for (let i = 0; i < this.markerCount; i++) {
      const myIcon = L.divIcon({
        html: this.generator.generateIcon(),
        className: 'my-div-icon',
        iconSize: [46, 46],
        iconAnchor: [23, 60],
        popupAnchor: [-3, -76],
        shadowSize: [68, 95],
        shadowAnchor: [22, 94]
      });
      const lat = Math.random() * (this.bnds._southWest.lat - this.bnds._northEast.lat) + this.bnds._northEast.lat;
      const lng = Math.random() * (this.bnds._southWest.lng - this.bnds._northEast.lng) + this.bnds._northEast.lng;
      L.marker([lat, lng], {icon: myIcon}).addTo(this.map);

    }
  }

  onMapReady(map: L.Map) {
    console.log(map.getBounds());
    this.map = map;
    this.bnds = map.getBounds();
    map.on('zoom drag', () => {
      this.bnds = map.getBounds();
    });
  }
}
