import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {AppRoutingModule} from './app-routing.module';
import { FormsModule } from '@angular/forms';
import {AppComponent} from './app.component';
import {IconGenerator} from '../utils/icon-generator';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LeafletModule.forRoot(),
    FormsModule
  ],
  providers: [IconGenerator],
  bootstrap: [AppComponent]
})
export class AppModule {
}
