import * as L from 'leaflet';

declare module 'leaflet' {

  namespace vectorGrid {

    function protobuf(v: any, k: any);

  }
}
